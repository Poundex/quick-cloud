//variable "k3s_server_node_count" {
//  type = number
//  default = 1
//}

variable "k3s_server_node_instance_type" {
  type = string
  default = "t3a.micro"
}

variable "vpc_private_subnets" {
  type = list(string)
}

variable "k3s_cluster_name" {
  type = string
  default = "lab-cluster"
}

variable "iam_loadbalancer_arns" {
  type = list(string)
}

variable "vpn_box_public_ip" {
  type = string
}