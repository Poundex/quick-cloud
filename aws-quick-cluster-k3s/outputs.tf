
output "kubeconfig_path" {
  depends_on = [null_resource.download_cluster_kubeconfig]
  value = abspath("${var.k3s_cluster_name}.kubeconfig")
}