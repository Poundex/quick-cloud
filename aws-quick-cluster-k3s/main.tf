resource "tls_private_key" "tls_private_key" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  key_name   = var.k3s_cluster_name
  public_key = tls_private_key.tls_private_key.public_key_openssh
}

resource "local_file" "cluster_keyfile" {
  filename = "${var.k3s_cluster_name}.pem"
  content = tls_private_key.tls_private_key.private_key_pem
  file_permission = "0600"
}

resource "aws_iam_role" "node_role" {
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "aws_lb_policy_attachment" {
  count = 4
  name = "aws_lb_policy_attachment"
  roles = [aws_iam_role.node_role.name]
  policy_arn = var.iam_loadbalancer_arns[count.index]
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "${var.k3s_cluster_name}-node-profile"
  role = aws_iam_role.node_role.name
}

resource "aws_instance" "k3s_server_node" {
  depends_on = [var.vpn_box_public_ip]

//  count = var.k3s_server_node_count
  ami = "ami-082335b69bcfdb15b"
  instance_type = var.k3s_server_node_instance_type
//  subnet_id = element(var.vpc_private_subnets, count)
  subnet_id = var.vpc_private_subnets[0]
  key_name = module.key_pair.this_key_pair_key_name
  iam_instance_profile = aws_iam_instance_profile.instance_profile.name
}

resource "random_password" "k3s_cluster_token" {
  length = 24
  special = false
}

resource "null_resource" "set_up_server_node" {

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = tls_private_key.tls_private_key.private_key_pem
    host = aws_instance.k3s_server_node.private_ip
  }

  provisioner "file" {
    source = "${path.module}/k3s-server-init.sh"
    destination = "~/k3s-server-init.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x k3s-server-init.sh",
      "sudo ./k3s-server-init.sh ${random_password.k3s_cluster_token.result}"
    ]
  }
}

resource "null_resource" "download_cluster_kubeconfig" {
  depends_on = [null_resource.set_up_server_node]

  provisioner "local-exec" {
    command = "scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -i ${var.k3s_cluster_name}.pem ubuntu@${aws_instance.k3s_server_node.private_ip}:/etc/rancher/k3s/k3s.yaml ./${var.k3s_cluster_name}.kubeconfig"
  }

  provisioner "local-exec" {
    command = "sed -i 's/127\\.0\\.0\\.1/${aws_instance.k3s_server_node.private_ip}/g' ${var.k3s_cluster_name}.kubeconfig"
  }
}


resource "aws_launch_template" "worker_node_lt" {
  name = "${var.k3s_cluster_name}-worker-template"
  depends_on = [null_resource.set_up_server_node]

  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile.name
  }

  image_id = "ami-082335b69bcfdb15b"
//  instance_market_options {
//    market_type = "spot"
//  }

  instance_type = "t3a.medium"
  key_name = module.key_pair.this_key_pair_key_name
  user_data = base64encode(<<EOF
#!/usr/bin/env bash
provider_id="$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)/$(curl -s http://169.254.169.254/latest/meta-data/instance-id)"
curl -sfL https://get.k3s.io | K3S_URL=https://${aws_instance.k3s_server_node.private_ip}:6443 K3S_TOKEN=${random_password.k3s_cluster_token.result} sh -s - --kubelet-arg cloud-provider=external --kubelet-arg "provider-id=aws:///$provider_id" --kubelet-arg "node-labels=node.kubernetes.io/lifecycle=spot"
EOF
)
}

resource "aws_autoscaling_group" "worker_asg" {
  max_size = 1
  min_size = 0
  desired_capacity = 1

//  availability_zones = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]

//  launch_template {
//    name = aws_launch_template.worker_node_lt.name
//    version = "$Latest"
//  }
  vpc_zone_identifier = toset(var.vpc_private_subnets)

  mixed_instances_policy {
    instances_distribution {
      on_demand_percentage_above_base_capacity = 0
    }
    launch_template {
      launch_template_specification {
        launch_template_id = aws_launch_template.worker_node_lt.id
      }

      override {
        instance_type = "t3a.large"
      }

      override {
        instance_type = "t3.large"
      }
    }
  }
}
