#!/usr/bin/env bash
provider_id="$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)/$(curl -s http://169.254.169.254/latest/meta-data/instance-id)"
curl -sfL https://get.k3s.io | sh -s - --write-kubeconfig-mode 644 --no-deploy traefik --kubelet-arg cloud-provider=external --kubelet-arg "provider-id=aws:///$provider_id" --agent-token "$1" --node-taint k3s-controlplane=true:NoExecute
