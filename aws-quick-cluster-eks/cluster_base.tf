data "aws_eks_cluster" "cluster" {
  name = module.cluster.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.cluster.cluster_id
}

module "cluster" {
  source = "terraform-aws-modules/eks/aws"
  cluster_name = var.eks_cluster_name
  cluster_version = var.eks_cluster_version
  subnets =  concat(var.vpc_public_subnets, var.vpc_private_subnets)
  vpc_id = var.vpc_id
  kubeconfig_aws_authenticator_env_variables = {
    "AWS_PROFILE": var.aws_profile
  }
  worker_groups_launch_template = [
    {
      name = "${var.eks_cluster_name}-workers"
      override_instance_types = [
        "t3.large",
        "t3a.large"]
      spot_instance_pools = 2
      asg_max_size = 3
      asg_desired_capacity = 3
      kubelet_extra_args = "--node-labels=node.kubernetes.io/lifecycle=spot"
      public_ip = false
      subnets = var.vpc_private_subnets
      additional_security_group_ids = [var.vpc_default_security_group_id]
    }
  ]
}

resource "aws_iam_policy_attachment" "aws_lb_policy_attachment" {
  count = 4
  name = "aws_lb_policy_attachment"
  roles = [module.cluster.worker_iam_role_name]
  policy_arn = var.iam_loadbalancer_arns[count.index]
}

