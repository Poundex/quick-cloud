
variable "eks_cluster_name" {
  type = string
  default = "lab-cluster"
}

variable "eks_cluster_version" {
  type = string
  default = "1.17"
}

variable "cluster_ingress_url" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_public_subnets" {
  type = list(string)
}

variable "vpc_private_subnets" {
  type = list(string)
}

variable "iam_loadbalancer_arns" {
  type = list(string)
}

variable "vpc_default_security_group_id" {
  type = string
}

variable "aws_profile" {
  type = string
}