variable "vpc_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_azs" {
  type = list(string)
  default = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
}

variable "vpc_private_subnet_cidrs" {
  type = list(string)
}

variable "vpc_public_subnet_cidrs" {
  type = list(string)
}

variable "vpc_public_subnets_extra_tags" {
  type = map(string)
  default = {}
}

variable "vpc_private_subnets_extra_tags" {
  type = map(string)
  default = {}
}

variable "cluster_name" {
  type = string
}
