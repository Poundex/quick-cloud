output "vpc_id" {
  value = module.vpc.vpc_id
}

output "public_subnets" {
  value = module.vpc.public_subnets
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "iam_loadbalancer_arns" {
  value = [aws_iam_policy.aws_lb_policy.arn, aws_iam_policy.aws_lb_policy2.arn,
    aws_iam_policy.aws_lb_policy3.arn, aws_iam_policy.aws_lb_policy4.arn]
}

output "vpc_default_security_group_id" {
  value = data.aws_security_group.sg_vpc_default.id
}