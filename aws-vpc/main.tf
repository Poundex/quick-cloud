module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs             = var.vpc_azs
  private_subnets = var.vpc_private_subnet_cidrs
  public_subnets  = var.vpc_public_subnet_cidrs

  enable_nat_gateway = true
  enable_vpn_gateway = false
  single_nat_gateway = true
  one_nat_gateway_per_az = false
  enable_dns_hostnames = true

  tags = {
    Terraform = "true"
    Environment = "dev"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}": "shared",
    "kubernetes.io/role/internal-elb": "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}": "shared"
  }
}

data "aws_security_group" "sg_vpc_default" {
  name = "default"
  vpc_id = module.vpc.vpc_id
}
