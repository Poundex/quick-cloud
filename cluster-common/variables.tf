variable "cluster_ingress_url" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "kubeconfig_path" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_default_security_group_id" {
  type = string
}