#!/usr/bin/env bash

kubectl --kubeconfig "$1" delete -n istio-system ingress ingress &
kubectl --kubeconfig "$1" delete -n istio-system istiooperator istio-mesh &

terraform state rm module.cluster_common.helm_release.smoketest_chart
terraform state rm module.cluster_common.helm_release.mesh_helper
terraform state rm module.cluster_common.helm_release.helm_alb_ingress
terraform state rm module.cluster_common.kubernetes_namespace.k8s_ns_istio_system
terraform state rm module.cluster_common.kubernetes_namespace.k8s_smoketest_ns
terraform state rm module.cluster_common.helm_release.istio_operator
terraform state rm module.cluster_common.data.aws_alb.cluster_ingress_alb
