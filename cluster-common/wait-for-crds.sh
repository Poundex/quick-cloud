#!/usr/bin/env bash
v=2
echo "Waiting for Istio CRDs"
for i in {1..35}; do
  echo wait
  kubectl --kubeconfig "$1" get crd virtualservices.networking.istio.io > /dev/null 2>&1
  if [ $? == 0 ]; then
    v=0
    break
  fi
  sleep 5
done

exit $v