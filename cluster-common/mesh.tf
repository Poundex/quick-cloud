module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> v2.0"

  domain_name  = var.cluster_ingress_url
  zone_id      = var.dns_zone_id

  subject_alternative_names = [
    "*.${var.cluster_ingress_url}"
  ]
}

resource "helm_release" "helm_alb_ingress" {
  name = "alb-ingress"
  namespace = "istio-system"
  chart = "aws-alb-ingress-controller"
  repository = "http://storage.googleapis.com/kubernetes-charts-incubator"

  set {
    name = "awsRegion"
    value = "eu-west-2"
  }

  set {
    name = "awsVpcID"
    value = var.vpc_id
  }

  set {
    name = "clusterName"
    value = var.cluster_name
  }
}

resource "kubernetes_namespace" "k8s_ns_istio_system" {
  metadata {
    name = "istio-system"
  }
}

resource "helm_release" "istio_operator" {
  name = "istio-operator"
  chart = " /home/pounder/Downloads/istio-1.7.1/manifests/charts/istio-operator"
  depends_on = [kubernetes_namespace.k8s_ns_istio_system]
//  namespace = "istio-system"

  set {
    name = "hub"
    value = "docker.io/istio"
  }

  set {
    name = "tag"
    value = "1.7.1"
  }
}

resource "helm_release" "mesh_helper" {
  name = "mesh-helper"
  chart = "../helm-mesh-helper"
  namespace = kubernetes_namespace.k8s_ns_istio_system.metadata[0].name
  depends_on = [helm_release.istio_operator]

  set {
    name = "clusterIngressUrl"
    value = var.cluster_ingress_url
  }

  set {
    name = "certificateArn"
    value = module.acm.this_acm_certificate_arn
  }

  set {
    name = "securityGroupId"
    value = var.vpc_default_security_group_id
  }
}

resource "null_resource" "wait_for_istio_crds" {
  depends_on = [helm_release.mesh_helper]

  provisioner "local-exec" {
    command = "${path.module}/wait-for-crds.sh ${var.kubeconfig_path}"
  }
}

data "aws_alb" "cluster_ingress_alb" {
  depends_on = [null_resource.wait_for_istio_crds, helm_release.helm_alb_ingress]
  tags = {"kubernetes.io/cluster/${var.cluster_name}": "owned"}
}

resource "aws_route53_record" "dns_cluster_ingress" {
  name = "*.${var.cluster_ingress_url}"
  type = "A"
  zone_id = var.dns_zone_id
  alias {
    evaluate_target_health = false
    name = data.aws_alb.cluster_ingress_alb.dns_name
    zone_id = data.aws_alb.cluster_ingress_alb.zone_id
  }
}

resource "helm_release" "helm_spot_helper" {
  depends_on = [null_resource.wait_for_istio_crds]
  chart = "stable/k8s-spot-termination-handler"
  name = "spot-termination-handler"
  namespace = "kube-system"

  set {
    name = "detachAsg"
    value = "true"
  }
}