resource "kubernetes_namespace" "k8s_smoketest_ns" {
  metadata {
    name = "smoketest"
  }
}

resource "helm_release" "smoketest_chart" {
  depends_on = [null_resource.wait_for_istio_crds]
  chart = "../helm-smoketest"
  name = "smoketest"
  namespace = kubernetes_namespace.k8s_smoketest_ns.metadata[0].name

  set {
    name = "clusterIngressUrl"
    value = var.cluster_ingress_url
  }
}