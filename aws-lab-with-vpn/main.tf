provider "aws" {
  region = var.aws_region
  profile = var.aws_profile
}

module "vpc" {
  source = "../aws-vpc"

  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
  vpc_private_subnet_cidrs = var.vpc_private_subnet_cidrs
  vpc_public_subnet_cidrs = var.vpc_public_subnet_cidrs

  cluster_name = var.cluster_name
}

module "vpn" {
  source = "../aws-quick-vpn"

  dns_zone_id = var.dns_zone_id
  sg_ingress_ip = var.sg_ingress_ip
  subnet_id = module.vpc.public_subnets[0]
  vpc_id = module.vpc.vpc_id
  wg_client_private_key = var.wg_client_private_key
  vpn_hostname = var.vpn_hostname
  vpc_cidr = var.vpc_cidr
  wg_peer_cidr = var.wg_peer_cidr
}

module "eks_cluster" {
  source = "../aws-quick-cluster-eks"
  count = var.cluster_type == "EKS" ? 1 : 0

  cluster_ingress_url = var.cluster_ingress_url
  dns_zone_id = var.dns_zone_id
  vpc_id = module.vpc.vpc_id
  vpc_public_subnets = module.vpc.public_subnets
  vpc_private_subnets = module.vpc.private_subnets
  iam_loadbalancer_arns = module.vpc.iam_loadbalancer_arns
  vpc_default_security_group_id = module.vpc.vpc_default_security_group_id
  aws_profile = var.aws_profile
}

module "k3s_cluster" {
  source = "../aws-quick-cluster-k3s"
  count = var.cluster_type == "k3s" ? 1 : 0

  vpc_private_subnets = module.vpc.private_subnets
  iam_loadbalancer_arns = module.vpc.iam_loadbalancer_arns
  vpn_box_public_ip = module.vpn.vpn_box_public_ip
}

provider "kubernetes" {
  config_path = var.cluster_type == "EKS" ? module.eks_cluster[0].kubeconfig_path : module.k3s_cluster[0].kubeconfig_path
}

provider "helm" {
  kubernetes {
    config_path = var.cluster_type == "EKS" ? module.eks_cluster[0].kubeconfig_path : module.k3s_cluster[0].kubeconfig_path
  }
}

module "cluster_common" {
  source = "../cluster-common"

  cluster_ingress_url = var.cluster_ingress_url
  cluster_name = var.cluster_name
  dns_zone_id = var.dns_zone_id
  kubeconfig_path = var.cluster_type == "EKS" ? module.eks_cluster[0].kubeconfig_path : module.k3s_cluster[0].kubeconfig_path
  vpc_id = module.vpc.vpc_id
  vpc_default_security_group_id = module.vpc.vpc_default_security_group_id
}