variable "vpc_name" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_azs" {
  type = list(string)
  default = ["eu-west-2a", "eu-west-2b", "eu-west-2c"]
}

variable "vpc_private_subnet_cidrs" {
  type = list(string)
}

variable "vpc_public_subnet_cidrs" {
  type = list(string)
}

variable "cluster_name" {
  type = string
  default = "lab-cluster"
}

variable "eks_cluster_version" {
  type = string
  default = "1.17"
}

variable "sg_ingress_ip" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "wg_client_private_key" {
  type = string
}

variable "cluster_ingress_url" {
  type = string
}

variable "vpn_hostname" {
  type = string
}

variable "wg_peer_cidr" {
  type = string
}

variable "cluster_type" {
  type = string
}

variable "aws_region" {
  type = string
  default = "eu-west-2"
}

variable "aws_profile" {
  type = string
}
