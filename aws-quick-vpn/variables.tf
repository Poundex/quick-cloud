variable "vpc_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

variable "sg_ingress_ip" {
  type = string
}

variable "dns_zone_id" {
  type = string
}

variable "wg_client_private_key" {
  type = string
}

variable "vpn_hostname" {
  type = string
}

variable "wg_peer_cidr" {
  type = string
}

variable "vpc_cidr" {
  type = string
}