#!/usr/bin/env bash
wg genkey | tee privatekey | wg pubkey > publickey
echo "==== SERVER Public Key: $(cat publickey)"
cat << EOF > /etc/wireguard/wg0.conf
[Interface]
PrivateKey = $(cat privatekey)
Address = 10.253.0.1/24
ListenPort = 51820
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o ens5 -j MASQUERADE; ip6tables -A FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -A POSTROUTING -o ens5 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o ens5 -j MASQUERADE; ip6tables -D FORWARD -i wg0 -j ACCEPT; ip6tables -t nat -D POSTROUTING -o ens5 -j MASQUERADE
SaveConfig = true

[Peer]
PublicKey = zZ180eUA6wuGSIocCago/sjhzx/Ak5JuJ9SngWhFt0g=
AllowedIPs = 10.253.0.100/32
EOF

echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
sysctl -p

