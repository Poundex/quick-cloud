output "vpn_box_public_ip" {
  value = aws_instance.ec2_vpn_box.public_ip
}

output "keypair_private_key" {
  value = tls_private_key.tls_private_key.private_key_pem
}