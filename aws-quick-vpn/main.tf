resource "tls_private_key" "tls_private_key" {
  algorithm = "RSA"
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"
  key_name   = "lab"
  public_key = tls_private_key.tls_private_key.public_key_openssh
}

//data "http" "my_public_ip" {
//  url = "https://ifconfig.co/json"
//  request_headers = {
//    Accept = "application/json"
//  }
//}
//
//locals {
//  ifconfig_co_json = jsondecode(data.http.my_public_ip.body)
//}

data "aws_security_group" "sg_vpc_default" {
  vpc_id = var.vpc_id
  name = "default"
}

resource "aws_security_group" "aws_sg_me" {
  name        = "me"
  description = "me"
  vpc_id      = var.vpc_id

  ingress {
    description = "All from me"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.sg_ingress_ip}/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ec2_vpn_box" {
  ami = "ami-082335b69bcfdb15b"
  instance_type = "t3a.micro"
  associate_public_ip_address = true
  key_name = module.key_pair.this_key_pair_key_name
  vpc_security_group_ids = [aws_security_group.aws_sg_me.id, data.aws_security_group.sg_vpc_default.id]
  subnet_id = var.subnet_id
}

resource "null_resource" "setup_wireguard" {

  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = tls_private_key.tls_private_key.private_key_pem
    host = aws_instance.ec2_vpn_box.public_ip
  }

  provisioner "file" {
    source = "${path.module}/wg-init.sh"
    destination = "~/wg-init.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get -y install wireguard",
      "chmod +x wg-init.sh",
      "sudo ./wg-init.sh",
      "sudo wg-quick up wg0",
    ]
  }
}

resource "aws_route53_record" "vpn_dns_record" {
  name = var.vpn_hostname
  type = "A"
  zone_id = var.dns_zone_id
  records = [aws_instance.ec2_vpn_box.public_ip]
  ttl = 300
}

resource "local_file" "vpn_box_keyfile" {
  filename = "vpn.pem"
  content = tls_private_key.tls_private_key.private_key_pem
  file_permission = "0600"
}

resource "null_resource" "download_wg_public_key" {
  depends_on = [local_file.vpn_box_keyfile, null_resource.setup_wireguard]

  provisioner "local-exec" {
    command = "scp -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -i vpn.pem ubuntu@${aws_instance.ec2_vpn_box.public_ip}:~/publickey ./wgpubkey"
  }
}

data "local_file" "wg_public_key" {
  depends_on = [null_resource.setup_wireguard, null_resource.download_wg_public_key]
  filename = "wgpubkey"
}

resource "local_file" "wg_client_config" {
  depends_on = [data.local_file.wg_public_key, null_resource.setup_wireguard, null_resource.download_wg_public_key]
  filename = "wg0.conf"

  content = <<EOF
[Interface]
PrivateKey = ${var.wg_client_private_key}
Address = ${var.wg_peer_cidr}

[Peer]
PublicKey = ${data.local_file.wg_public_key.content}
Endpoint = ${aws_instance.ec2_vpn_box.public_ip}:51820
AllowedIPs = ${var.wg_peer_cidr}, ${var.vpc_cidr}
EOF
}

resource "null_resource" "set_up_wg_client" {
  depends_on = [local_file.wg_client_config]

  provisioner "local-exec" {
    command = "./wg-client-init.sh"
  }
}